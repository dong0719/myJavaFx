package sample;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.event.ActionEvent;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.util.Duration;

public class Controller {
    public Button btn1;
    public Label label1;

    public void daojishi(ActionEvent actionEvent) {
        btn1.setDisable(true);
        clock();
    }

    private Timeline animation;
    private String S = "";
    private int tmp = 300;

    public void clock() {
        animation = new Timeline(new KeyFrame(Duration.millis(1000), e -> timelabel()));
        animation.setCycleCount(Timeline.INDEFINITE);
        animation.play();
        label1.setAlignment(Pos.CENTER);
    }

    public void timelabel() {
        tmp--;
        S = tmp + "";
        label1.setText(S);
    }
}
